# README #

requires the three Jars to be built for each of the components.

The components are :

discover-server is a Eureka Server
zuul-server is a zuul gateway
categoriser is the microservice that does the work

The easiest is to import them into 3 separate projects and springboot run them
or use maven

go into each of the compenents and run

mvn package

then go into the target directory of the component, and run:
java -jar  xxxx-0.0.1-SNAPSHOT.jar
where xxxx  is the name of the component 


# testing with curl #

To categorise an companies accounts transactions for coffee & travel use:

curl -X GET \
  http://localhost:8081/api/categorise \
  -H 'Authorization: Bearer <BEARER TOKEN>' \
  -H 'account: <ACCOUNT NAME eg :fakeAcc77>' \
  -H 'cache-control: no-cache' \
  -H 'companyId: <COMPANY ID eg :2>' \
  -H 'x-api-key: <API KEY>' \
  -H 'x-partner-id: <PARTNER ID>'
  
  to categorise an companies accounts transactions for custom filter use :
 
 curl -X POST \
  http://localhost:8081/api/categorise \
  -H 'Authorization: Bearer <BEARER TOKEN>' \
  -H 'Content-Type: application/json' \
  -H 'account: <ACCOUNT NAME eg :fakeAcc77>' \
  -H 'cache-control: no-cache' \
  -H 'companyId: <COMPANY ID eg :2>' \
  -H 'x-api-key: <API KEY>' \
  -H 'x-partner-id: <PARTNER ID>'
  -d '["coffee","banking"]'
  

 to create or update a lable the following call can be used:
  
  curl -X POST \
  http://localhost:8081/api/label \
  -H 'Authorization: Bearer  <BEARER TOKEN>' \
  -H 'Content-Type: application/json' \
  -H 'x-api-key: <API KEY>' \
  -H 'x-partner-id: <PARTNER ID>' \
  -H 'cache-control: no-cache' \
  -d '{"category":"banking","keyword":"cashpoint","confidence":0.70}'
  

 if a category does not esxist then a category will be created. Categorisation works by analysing the description, and looking for keywords, different keywords have different levels of confidence. One we have more than 66% confidence we will assume that it is a candidate for that category. To recategorise a transaction you need to adjust the confidence of the keywords in the descripton to either make it in or not in the category of your choosing

  
 to get a label the following get can be used:
  
  curl -X GET \
  http://localhost:8081/api/label/<CATEGORY>/<KEYWORD> \
  -H 'Authorization: Bearer  <BEARER TOKEN>' \
  -H 'Content-Type: application/json' \
  -H 'cache-control: no-cache' \
  -H 'x-api-key: GJUFYCTp98vV63mKl9Pp5i02cXjUXvp3rYa9bX4b' \
  -H 'x-partner-id: 5111acc7-c9b3-4d2a-9418-16e97b74b1e6'
  
  
   