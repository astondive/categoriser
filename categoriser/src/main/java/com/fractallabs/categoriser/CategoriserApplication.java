package com.fractallabs.categoriser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.UiConfiguration;
import springfox.documentation.swagger.web.UiConfigurationBuilder;
import org.apache.commons.lang.StringUtils;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@EnableDiscoveryClient
@SpringBootApplication
public class CategoriserApplication {

	public static void main(String[] args) {
		SpringApplication.run(CategoriserApplication.class, args);
	}

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.fractallabs"))  // // Generate API of EndPoints which is available inside defined package
				.paths(PathSelectors.any())   // for all EndPoints
				.build();             // create object
	}

	@Bean
	public UiConfiguration uiConfig()
	{
		return UiConfigurationBuilder.builder() //
				.displayRequestDuration( true ) //
				.validatorUrl( StringUtils.EMPTY ) // Disable the validator to avoid "Error" at the bottom of the Swagger UI page
				.build();
	}
}
