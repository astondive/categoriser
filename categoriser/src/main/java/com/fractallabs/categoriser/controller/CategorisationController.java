package com.fractallabs.categoriser.controller;

import com.fractallabs.categoriser.model.Category;
import com.fractallabs.categoriser.model.CategoryLabel;
import com.fractallabs.categoriser.model.FlatLabel;
import com.fractallabs.categoriser.model.transaction.Result;
import com.fractallabs.categoriser.services.CategorisingService;
import com.fractallabs.categoriser.services.TransactionService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.Api;
import java.util.*;

@RestController
@Api(value = "API for Categorisation")
public class CategorisationController {
	private static final Logger log = LogManager.getLogger(CategorisationController.class);

	@Autowired
	TransactionService tServ;

	@Autowired
	CategorisingService cServ;

	@GetMapping(value = "/categorise")
	synchronized public String categorise(
			@RequestHeader(required = true, value = "x-api-key") String apiKey,
			@RequestHeader(required = true, value = "x-partner-id") String partnerId,
			@RequestHeader(required = true, value = "companyId") String companyId,
			@RequestHeader(required = true, value = "x-secure-token") String secureToken,
			@RequestHeader(required = true, value = "account") String account,
			@RequestHeader(required = true, value = "authorization") String bearerToken) throws Exception {
		log.info("categorise called");
		validateSecurityToken(secureToken);
		tServ.clear();
		tServ.fetchTransactions(companyId, account, bearerToken, apiKey, partnerId);
		List<Result> transactions = tServ.getAllTransactions();
		final String[] filter =  {"coffee","travel"};
		return cServ.filterCategory(transactions,filter);
	}


	@PostMapping(value = "/categorise")
	synchronized public String categorisebyFilter(
			@RequestHeader(required = true, value = "x-api-key") String apiKey,
			@RequestHeader(required = true, value = "x-partner-id") String partnerId,
			@RequestHeader(required = true, value = "companyId") String companyId,
			@RequestHeader(required = true, value = "x-secure-token") String secureToken,
			@RequestHeader(required = true, value = "account") String account,
			@RequestHeader(required = true, value = "authorization") String bearerToken,
			@RequestBody String[] filter
	) throws Exception {
		log.info("categorise called with filter"+filter.length);
		validateSecurityToken(secureToken);
		tServ.clear();

		tServ.fetchTransactions(companyId, account, bearerToken, apiKey, partnerId);
		List<Result> transactions = tServ.getAllTransactions();
		return cServ.filterCategory(transactions,filter);
	}

	private void validateSecurityToken(String secureToken) throws Exception {
		// these tokens should be stored in something like HashiCorps Vault, and pulled before sending and on
		// verification so the tokens can be rolled.
		if (!secureToken.equals("fvklvaosihOHFoih0893247283hrwnhsdjhcksjdfh4u8yfp9784fh49iudehf")) {
			log.error("Bad request");
			throw new Exception("Invalid security token");
		}
	}

	@PostMapping(value = "/label")
	private FlatLabel newLabel(
			@RequestHeader(required = true, value = "x-secure-token") String secureToken,
			@RequestBody FlatLabel label)throws Exception{
		validateSecurityToken(secureToken) ;
		Category category = cServ.getCategory(label.getCategory());
		if(!cServ.getAllCategories().contains(category)){
			category = new Category(label.getCategory());
			cServ.addCategory(category);
		}
		cServ.addLabel(category,label.getKeyword(),label.getConfidence());

		return cServ.getLabel(category,label.getKeyword());
	}

	@GetMapping(value = "/label/{categoryName}/{keyword}")
	private FlatLabel getLabel(
			@RequestHeader(required = true, value = "x-secure-token") String secureToken,
			@PathVariable String categoryName,
			@PathVariable String keyword) throws Exception{
		validateSecurityToken(secureToken) ;
		Category category = cServ.getCategory(categoryName);
		return cServ.getLabel(category,keyword);
	}
}
