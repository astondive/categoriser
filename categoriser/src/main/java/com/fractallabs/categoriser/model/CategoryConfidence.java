package com.fractallabs.categoriser.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CategoryConfidence {
	Category category;
	Double confidence;
}
