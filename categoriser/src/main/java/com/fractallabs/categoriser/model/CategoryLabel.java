package com.fractallabs.categoriser.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CategoryLabel {
	Category category;
	String keyword;
	Double confidence;
}
