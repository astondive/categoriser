package com.fractallabs.categoriser.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FlatLabel {
	public FlatLabel(CategoryLabel categoryLabel) {
		this.category = categoryLabel.getCategory().getName();
		this.keyword = categoryLabel.getKeyword();
		this.confidence = categoryLabel.getConfidence();
	}

	String category;
	String keyword;
	Double confidence;
}
