package com.fractallabs.categoriser.model.transaction;

import lombok.Data;

@Data
public class Merchant {
	private String name;
}
