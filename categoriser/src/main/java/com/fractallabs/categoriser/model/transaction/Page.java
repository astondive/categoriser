package com.fractallabs.categoriser.model.transaction;

import lombok.Data;

import java.util.List;

@Data
public class Page {
	public List<Result> results = null;
	public Integer pageNo;
	public Integer noPages;

}
