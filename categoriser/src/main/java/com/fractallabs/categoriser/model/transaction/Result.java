package com.fractallabs.categoriser.model.transaction;

import lombok.Data;

@Data
public class Result {
	private Integer bankId;
	private String accountId;
	private String transactionId;
	private String bookingDate;
	private String reference;
	private String transactionCode;
	private String transactionSubCode;
	private String proprietaryCode;
	private String proprietarySubCode;
	private String description;
	private String amount;
	private String currencyCode;
	private String type;
	private String status;
	private Merchant merchant;
	private Integer companyId;
}
