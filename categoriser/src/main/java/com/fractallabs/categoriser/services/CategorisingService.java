package com.fractallabs.categoriser.services;

import com.fractallabs.categoriser.model.Category;
import com.fractallabs.categoriser.model.CategoryConfidence;
import com.fractallabs.categoriser.model.CategoryLabel;
import com.fractallabs.categoriser.model.FlatLabel;
import com.fractallabs.categoriser.model.transaction.Result;
import com.google.gson.Gson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

import java.text.NumberFormat;
import java.util.*;

@Service
public class CategorisingService {

	static final Double CONFIDENCE_TRESHOLD = 0.66;

	private static final Logger log = LogManager.getLogger(CategorisingService.class);
	Map<Category, Double> categoriesConfidence;
	Map<String, Map<Category, CategoryLabel>> categoryLabels = new HashMap<>();

	public CategorisingService() {
		log.info("Categorisation service initialising");
		setupCategorisingLabels();
	}

	private void setupCategorisingLabels() {

		/*
		 *  This should really be pulled from a database after
		 *  various texts have been labelled by a categorisation
		 *  expert as part of an NLP exercise
		 *
		 */

		Category coffee = new Category("coffee");
		Category travel = new Category("travel");

		categoriesConfidence = categoriesConfidence = new HashMap<>();
		categoriesConfidence.put(coffee, 0.0);
		categoriesConfidence.put(travel, 0.0);

		categoryLabels = new HashMap();
		addLabel(coffee, "starbucks", 0.65);
		addLabel(coffee, "stn", 0.05);
		addLabel(coffee, "coffee", 0.9);
		addLabel(coffee, "costa", 0.65);
		addLabel(coffee, "latte", 0.2);
		addLabel(coffee, "java", 0.05);
		addLabel(travel, "tfl", 0.80);
		addLabel(travel, "rail", 0.50);
		addLabel(travel, "station", 0.05);
		addLabel(travel, "stn", 0.05);
		addLabel(travel, "bus", 0.45);
		addLabel(travel, "taxi", 0.52);
		addLabel(travel, "ticket", 0.22);
		addLabel(travel, "journey", 0.22);
		addLabel(travel, "journe", 0.15);
	}

	public void addLabel(Category category, String keyword, Double confidence) {
		Map<Category, CategoryLabel> keywordMap = categoryLabels.getOrDefault(keyword, new HashMap<>());
		CategoryLabel categoryLabel = keywordMap.getOrDefault(category, new CategoryLabel(category, keyword, confidence));
		categoryLabel.setConfidence(confidence); // always override the value
		keywordMap.put(category, categoryLabel);
		categoryLabels.put(keyword, keywordMap);
	}

	@Nullable
	public CategoryConfidence categorise(String text) {
		resetCategoryConfidence();
		splitToWordsAndCategorise(text);
		return findMostRelevantCategory();
	}

	public Set<Category> getAllCategories(){
		return categoriesConfidence.keySet();
	}

	private void resetCategoryConfidence() {
		for (Category category : categoriesConfidence.keySet()) {
			categoriesConfidence.put(category, 0.0);
		}
	}

	private CategoryConfidence findMostRelevantCategory() {
		CategoryConfidence mostRelevantCategory = null;
		for (Category category : categoriesConfidence.keySet()) {
			final Double confidence = categoriesConfidence.get(category);
			if (confidence > CONFIDENCE_TRESHOLD) {
				if (mostRelevantCategory == null || categoriesConfidence.get(mostRelevantCategory) < confidence) {
					mostRelevantCategory = new CategoryConfidence(category,confidence);
				}
			}
		}
		return mostRelevantCategory;
	}

	private void splitToWordsAndCategorise(String text) {
		String[] words = text.split("\\W+");
		for (String word : words) {
			categoriseWord(word.toLowerCase());
		}
	}

	private void categoriseWord(String word) {
		final CategoryLabel zero = new CategoryLabel(null, "", 0.0);
		Map<Category, CategoryLabel> categoryMap = categoryLabels.get(word);
		if (categoryMap != null) {
			for (Category category : categoriesConfidence.keySet()) {
				Double confidence = categoriesConfidence.put(
						category, categoriesConfidence.get(category) +
								categoryMap.getOrDefault(category, zero).getConfidence());
			}
		}
	}


	public String filterCategory(List<Result> transactions, String[] filter) {
		return filterCategory(categoriseResults(transactions),filter);
	}

	public String filterCategory(Map<Category, Integer> categoryIntegerMap, String[] filter) {
		return filterCategory(categoryIntegerMap, new HashSet<>(Arrays.asList(filter)));
	}

	private String filterCategory(Map<Category, Integer> categoryIntegerMap, Set<String> filter) {
		Map<String,Integer> results = new HashMap<>();

		for (Category category : categoryIntegerMap.keySet()){
			log.info(category.getName()+"="+categoryIntegerMap.get(category) );
			if (filter.contains(category.getName())){
				results.put(category.getName(),categoryIntegerMap.get(category));
			}
		}
		Gson gson = new Gson();
		return gson.toJson(results);
	}

	public Map<Category, Integer> categoriseResults(List<Result> transactions) {
		Map<Category, Integer> occurrences = new HashMap<>();

		NumberFormat defaultFormat = NumberFormat.getPercentInstance();
		defaultFormat.setMinimumFractionDigits(1);

		for (Result result : transactions) {
			CategoryConfidence category = categorise(result.getDescription());
			if (category != null) {
				if (getAllCategories().contains(category.getCategory())) {
					int count = occurrences.getOrDefault(category.getCategory(), 0);
					occurrences.put(category.getCategory(), ++count);
				}
				log.info("Categorising:" + result.getDescription() +
						" as " + category.getCategory().getName() +
						" with confidence " + defaultFormat.format(Math.min(category.getConfidence(), 1.0)));
			} else {
				log.info("Not Categorising:" + result.getDescription());
			}
		}
		return occurrences;
	}

	public void addCategory(Category category) {
		if (!categoriesConfidence.containsKey(category)) {
			categoriesConfidence.put(category, 0.0);
		}
	}

	public FlatLabel getLabel(Category category, String keyword) {
		Map<Category, CategoryLabel> labels = categoryLabels.get(keyword);
		return new FlatLabel(labels.get(category));
	}

	public Category getCategory(String categoryName) {
		for (Category category :categoriesConfidence.keySet()){
			if (category.getName().equalsIgnoreCase(categoryName)){
				return category;
			}
		}
		return null;
	}
}
