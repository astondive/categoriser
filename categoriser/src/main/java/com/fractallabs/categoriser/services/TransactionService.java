package com.fractallabs.categoriser.services;

import com.fractallabs.categoriser.model.transaction.Page;
import com.fractallabs.categoriser.model.transaction.Result;
import com.google.gson.Gson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class TransactionService {

	private static final Logger log = LogManager.getLogger(TransactionService.class);

	ArrayList<Page> transactions = new ArrayList();

	public void importPageOfTransactions(String json){
		Gson gson = new Gson();
		transactions.add(gson.fromJson(json,Page.class));
	}

	public List<Result> getAllTransactions(){
		return transactions.stream().flatMap(page -> page.results.stream()).collect(Collectors.toList());
	}

	public void fetchTransactions(
			String companyId,
			String account,
			String bearerToken,
			String apiKey,
			String parterId) {

		RestTemplate restTemplate = new RestTemplate();
		Gson gson = new Gson();
		String url = "https://r7p2rhg4ji.execute-api.eu-west-1.amazonaws.com/v1/banking/" +
				companyId + "/accounts/" + account + "/transactions";
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", bearerToken);
		headers.set("x-api-key","GJUFYCTp98vV63mKl9Pp5i02cXjUXvp3rYa9bX4b");
		headers.set("x-partner-id","5111acc7-c9b3-4d2a-9418-16e97b74b1e6");
		headers.set("companyId",companyId);

		log.info("BearerToken = "+bearerToken);
		try {
			HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
			ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
			log.info("Result - status ("+ response.getStatusCode() + ") has body: " + response.hasBody());

			String body = response.getBody();
			logResponseHeaders(response);
			log.info(body);
			transactions.add(gson.fromJson(body,Page.class));
		}
		catch (Exception e) {
			log.error(e.getMessage(),e);
		}

	}

	private void logResponseHeaders(ResponseEntity<String> response) {
		Set<Map.Entry<String, List<String>>> responseHeaders = response.getHeaders().entrySet();
		responseHeaders.forEach(f ->
		{
			StringBuilder sb = new StringBuilder("header ");
			sb.append(f.getKey());
			String sep = (" : {");
			for(String value : f.getValue()){
				sb.append(sep);
				sb.append(value);
				sep = ",";
			}
			sb.append("}");
			log.info(sb.toString());


		});
	}

	public void clear() {
		transactions.clear();
	}
}
