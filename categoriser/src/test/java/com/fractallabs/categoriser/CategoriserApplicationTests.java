package com.fractallabs.categoriser;

import com.fractallabs.categoriser.model.CategoryConfidence;
import com.fractallabs.categoriser.model.transaction.Result;
import com.fractallabs.categoriser.services.CategorisingService;
import com.fractallabs.categoriser.services.TransactionService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.NumberFormat;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CategoriserApplicationTests {


	private static final Logger log = LogManager.getLogger(CategoriserApplicationTests.class);

	static String transactions = "{\n" +
			"    \"results\": [\n" +
			"        {\n" +
			"            \"bankId\": 2,\n" +
			"            \"accountId\": \"fakeAcc77\",\n" +
			"            \"transactionId\": \"0ef942ea-d3ad-4f25-857b-4d4bb7f912d8\",\n" +
			"            \"bookingDate\": \"2018-07-24\",\n" +
			"            \"reference\": \"\",\n" +
			"            \"transactionCode\": \"\",\n" +
			"            \"transactionSubCode\": \"\",\n" +
			"            \"proprietaryCode\": \"\",\n" +
			"            \"proprietarySubCode\": \"\",\n" +
			"            \"description\": \"STARBUCKS COFFEE Simple\",\n" +
			"            \"amount\": \"1000.0\",\n" +
			"            \"currencyCode\": \"GBP\",\n" +
			"            \"type\": \"DEBIT\",\n" +
			"            \"status\": \"BOOKED\",\n" +
			"            \"merchant\": {\n" +
			"                \"name\": \"Starbucks\"\n" +
			"            },\n" +
			"            \"companyId\": 144\n" +
			"        },\n" +
			"        {\n" +
			"            \"bankId\": 2,\n" +
			"            \"accountId\": \"fakeAcc77\",\n" +
			"            \"transactionId\": \"25a4d256-094f-4637-bea0-5f82760674b0\",\n" +
			"            \"bookingDate\": \"2017-04-28\",\n" +
			"            \"reference\": \"\",\n" +
			"            \"transactionCode\": \"\",\n" +
			"            \"transactionSubCode\": \"\",\n" +
			"            \"proprietaryCode\": \"\",\n" +
			"            \"proprietarySubCode\": \"\",\n" +
			"            \"description\": \"INT'L 0020481195 AMAZON UK RETAIL A AMAZON.CO.UK\",\n" +
			"            \"amount\": \"23.12\",\n" +
			"            \"currencyCode\": \"GBP\",\n" +
			"            \"type\": \"DEBIT\",\n" +
			"            \"status\": \"BOOKED\",\n" +
			"            \"merchant\": {\n" +
			"                \"name\": \"AMAZON UK RETAIL\"\n" +
			"            },\n" +
			"            \"companyId\": 19\n" +
			"        },\n" +
			"        {\n" +
			"            \"bankId\": 2,\n" +
			"            \"accountId\": \"fakeAcc77\",\n" +
			"            \"transactionId\": \"2eeb4b27-eac8-4d6c-a1cb-18ad8c57514d\",\n" +
			"            \"bookingDate\": \"2017-09-02\",\n" +
			"            \"reference\": \"\",\n" +
			"            \"transactionCode\": \"\",\n" +
			"            \"transactionSubCode\": \"\",\n" +
			"            \"proprietaryCode\": \"\",\n" +
			"            \"proprietarySubCode\": \"\",\n" +
			"            \"description\": \"TFL.GOV.UK/CP TFL TRAVEL CH\",\n" +
			"            \"amount\": \"4.69\",\n" +
			"            \"currencyCode\": \"GBP\",\n" +
			"            \"type\": \"DEBIT\",\n" +
			"            \"status\": \"BOOKED\",\n" +
			"            \"merchant\": {\n" +
			"                \"name\": \"TFL.GOV.UK\"\n" +
			"            },\n" +
			"            \"companyId\": 142\n" +
			"        },\n" +
			"        {\n" +
			"            \"bankId\": 2,\n" +
			"            \"accountId\": \"fakeAcc77\",\n" +
			"            \"transactionId\": \"4661c026-4605-4773-9b49-b781ed9909ca\",\n" +
			"            \"bookingDate\": \"2018-07-24\",\n" +
			"            \"reference\": \"\",\n" +
			"            \"transactionCode\": \"\",\n" +
			"            \"transactionSubCode\": \"\",\n" +
			"            \"proprietaryCode\": \"\",\n" +
			"            \"proprietarySubCode\": \"\",\n" +
			"            \"description\": \"STARBUCKS POULTRY STN\",\n" +
			"            \"amount\": \"2000.0\",\n" +
			"            \"currencyCode\": \"GBP\",\n" +
			"            \"type\": \"DEBIT\",\n" +
			"            \"status\": \"BOOKED\",\n" +
			"            \"merchant\": {\n" +
			"                \"name\": \"Starbucks\"\n" +
			"            },\n" +
			"            \"companyId\": 44\n" +
			"        },\n" +
			"        {\n" +
			"            \"bankId\": 2,\n" +
			"            \"accountId\": \"fakeAcc77\",\n" +
			"            \"transactionId\": \"4a503bc0-c7f5-4a5f-9ae7-7269ce9d951e\",\n" +
			"            \"bookingDate\": \"2017-11-07\",\n" +
			"            \"reference\": \"\",\n" +
			"            \"transactionCode\": \"\",\n" +
			"            \"transactionSubCode\": \"\",\n" +
			"            \"proprietaryCode\": \"\",\n" +
			"            \"proprietarySubCode\": \"\",\n" +
			"            \"description\": \"INT'L 0020481195 AMAZON UK RETAIL A AMAZON.CO.UK\",\n" +
			"            \"amount\": \"46.99\",\n" +
			"            \"currencyCode\": \"GBP\",\n" +
			"            \"type\": \"DEBIT\",\n" +
			"            \"status\": \"BOOKED\",\n" +
			"            \"merchant\": {\n" +
			"                \"name\": \"AMAZON UK RETAIL\"\n" +
			"            },\n" +
			"            \"companyId\": 32\n" +
			"        },\n" +
			"        {\n" +
			"            \"bankId\": 2,\n" +
			"            \"accountId\": \"fakeAcc77\",\n" +
			"            \"transactionId\": \"5875d68d-d53c-4f4b-a447-1606059fe57f\",\n" +
			"            \"bookingDate\": \"2017-11-03\",\n" +
			"            \"reference\": \"\",\n" +
			"            \"transactionCode\": \"\",\n" +
			"            \"transactionSubCode\": \"\",\n" +
			"            \"proprietaryCode\": \"\",\n" +
			"            \"proprietarySubCode\": \"\",\n" +
			"            \"description\": \"BT MOBILE\",\n" +
			"            \"amount\": \"34.6\",\n" +
			"            \"currencyCode\": \"GBP\",\n" +
			"            \"type\": \"DEBIT\",\n" +
			"            \"status\": \"BOOKED\",\n" +
			"            \"merchant\": {\n" +
			"                \"name\": \"BT MOBILE\"\n" +
			"            }\n" +
			"        },\n" +
			"        {\n" +
			"            \"bankId\": 2,\n" +
			"            \"accountId\": \"fakeAcc77\",\n" +
			"            \"transactionId\": \"58b87c57-b3e4-4b5b-9423-c0f13b9f2eb1\",\n" +
			"            \"bookingDate\": \"2018-07-22\",\n" +
			"            \"reference\": \"\",\n" +
			"            \"transactionCode\": \"\",\n" +
			"            \"transactionSubCode\": \"\",\n" +
			"            \"proprietaryCode\": \"\",\n" +
			"            \"proprietarySubCode\": \"\",\n" +
			"            \"description\": \"STARBUCKS VICTORIA STN\",\n" +
			"            \"amount\": \"850000.0\",\n" +
			"            \"currencyCode\": \"GBP\",\n" +
			"            \"type\": \"DEBIT\",\n" +
			"            \"status\": \"BOOKED\",\n" +
			"            \"merchant\": {\n" +
			"                \"name\": \"Starbucks\"\n" +
			"            }\n" +
			"        },\n" +
			"        {\n" +
			"            \"bankId\": 2,\n" +
			"            \"accountId\": \"fakeAcc77\",\n" +
			"            \"transactionId\": \"698f1d0c-5b2b-449a-a668-c862c0aaef39\",\n" +
			"            \"bookingDate\": \"2017-12-16\",\n" +
			"            \"reference\": \"\",\n" +
			"            \"transactionCode\": \"\",\n" +
			"            \"transactionSubCode\": \"\",\n" +
			"            \"proprietaryCode\": \"\",\n" +
			"            \"proprietarySubCode\": \"\",\n" +
			"            \"description\": \"LONDON TAXI JOURNE LONDON  E1\",\n" +
			"            \"amount\": \"6.51\",\n" +
			"            \"currencyCode\": \"GBP\",\n" +
			"            \"type\": \"DEBIT\",\n" +
			"            \"status\": \"BOOKED\",\n" +
			"            \"merchant\": {\n" +
			"                \"name\": \"TAXI CABS\"\n" +
			"            }\n" +
			"        },\n" +
			"        {\n" +
			"            \"bankId\": 2,\n" +
			"            \"accountId\": \"fakeAcc77\",\n" +
			"            \"transactionId\": \"73e752d3-e4a6-4bc5-b7ff-3d7b1f674404\",\n" +
			"            \"bookingDate\": \"2017-08-23\",\n" +
			"            \"reference\": \"\",\n" +
			"            \"transactionCode\": \"\",\n" +
			"            \"transactionSubCode\": \"\",\n" +
			"            \"proprietaryCode\": \"\",\n" +
			"            \"proprietarySubCode\": \"\",\n" +
			"            \"description\": \"TOTAL CHARGES TO 05MAR2017\",\n" +
			"            \"amount\": \"4.12\",\n" +
			"            \"currencyCode\": \"GBP\",\n" +
			"            \"type\": \"DEBIT\",\n" +
			"            \"status\": \"BOOKED\",\n" +
			"            \"merchant\": {\n" +
			"                \"name\": \"HSBC PLC\"\n" +
			"            }\n" +
			"        },\n" +
			"        {\n" +
			"            \"bankId\": 2,\n" +
			"            \"accountId\": \"fakeAcc77\",\n" +
			"            \"transactionId\": \"75660071-9994-4457-93d8-002f6a080e17\",\n" +
			"            \"bookingDate\": \"2017-10-02\",\n" +
			"            \"reference\": \"\",\n" +
			"            \"transactionCode\": \"\",\n" +
			"            \"transactionSubCode\": \"\",\n" +
			"            \"proprietaryCode\": \"\",\n" +
			"            \"proprietarySubCode\": \"\",\n" +
			"            \"description\": \"COSTA COFFEE\",\n" +
			"            \"amount\": \"6.2\",\n" +
			"            \"currencyCode\": \"GBP\",\n" +
			"            \"type\": \"DEBIT\",\n" +
			"            \"status\": \"BOOKED\",\n" +
			"            \"merchant\": {\n" +
			"                \"name\": \"COSTA COFFEE\"\n" +
			"            }\n" +
			"        },\n" +
			"        {\n" +
			"            \"bankId\": 2,\n" +
			"            \"accountId\": \"fakeAcc77\",\n" +
			"            \"transactionId\": \"8106907d-3e5f-4b90-be18-3e62df18a83c\",\n" +
			"            \"bookingDate\": \"2018-07-24\",\n" +
			"            \"reference\": \"\",\n" +
			"            \"transactionCode\": \"\",\n" +
			"            \"transactionSubCode\": \"\",\n" +
			"            \"proprietaryCode\": \"\",\n" +
			"            \"proprietarySubCode\": \"\",\n" +
			"            \"description\": \"Cashpoint Holborn\",\n" +
			"            \"amount\": \"10000.0\",\n" +
			"            \"currencyCode\": \"GBP\",\n" +
			"            \"type\": \"DEBIT\",\n" +
			"            \"status\": \"BOOKED\",\n" +
			"            \"merchant\": {\n" +
			"                \"name\": \"HSBC ATM\"\n" +
			"            }\n" +
			"        },\n" +
			"        {\n" +
			"            \"bankId\": 2,\n" +
			"            \"accountId\": \"fakeAcc77\",\n" +
			"            \"transactionId\": \"8439173b-4ef3-44c4-82fe-282bbae35dba\",\n" +
			"            \"bookingDate\": \"2017-04-02\",\n" +
			"            \"reference\": \"\",\n" +
			"            \"transactionCode\": \"\",\n" +
			"            \"transactionSubCode\": \"\",\n" +
			"            \"proprietaryCode\": \"\",\n" +
			"            \"proprietarySubCode\": \"\",\n" +
			"            \"description\": \"Non-Sterling Page Fee\",\n" +
			"            \"amount\": \"29.33\",\n" +
			"            \"currencyCode\": \"GBP\",\n" +
			"            \"type\": \"DEBIT\",\n" +
			"            \"status\": \"BOOKED\",\n" +
			"            \"merchant\": {\n" +
			"                \"name\": \"HSBC PLC\"\n" +
			"            }\n" +
			"        },\n" +
			"        {\n" +
			"            \"bankId\": 2,\n" +
			"            \"accountId\": \"fakeAcc77\",\n" +
			"            \"transactionId\": \"8e4d2111-859c-49b1-8557-d5f52fdf6685\",\n" +
			"            \"bookingDate\": \"2018-01-13\",\n" +
			"            \"reference\": \"\",\n" +
			"            \"transactionCode\": \"\",\n" +
			"            \"transactionSubCode\": \"\",\n" +
			"            \"proprietaryCode\": \"\",\n" +
			"            \"proprietarySubCode\": \"\",\n" +
			"            \"description\": \"Non-Sterling Page Fee\",\n" +
			"            \"amount\": \"20.55\",\n" +
			"            \"currencyCode\": \"GBP\",\n" +
			"            \"type\": \"DEBIT\",\n" +
			"            \"status\": \"BOOKED\",\n" +
			"            \"merchant\": {\n" +
			"                \"name\": \"HSBC PLC\"\n" +
			"            }\n" +
			"        },\n" +
			"        {\n" +
			"            \"bankId\": 2,\n" +
			"            \"accountId\": \"fakeAcc77\",\n" +
			"            \"transactionId\": \"9619d91f-954e-4aff-a89e-0a02dc71eff0\",\n" +
			"            \"bookingDate\": \"2018-07-25\",\n" +
			"            \"reference\": \"\",\n" +
			"            \"transactionCode\": \"\",\n" +
			"            \"transactionSubCode\": \"\",\n" +
			"            \"proprietaryCode\": \"\",\n" +
			"            \"proprietarySubCode\": \"\",\n" +
			"            \"description\": \"RETAIL LTD London\",\n" +
			"            \"amount\": \"2000.0\",\n" +
			"            \"currencyCode\": \"GBP\",\n" +
			"            \"type\": \"DEBIT\",\n" +
			"            \"status\": \"BOOKED\",\n" +
			"            \"merchant\": {\n" +
			"                \"name\": \"LAKERS\"\n" +
			"            }\n" +
			"        },\n" +
			"        {\n" +
			"            \"bankId\": 2,\n" +
			"            \"accountId\": \"fakeAcc77\",\n" +
			"            \"transactionId\": \"a57a9f04-018d-4e23-8be1-15cd5a61ffc6\",\n" +
			"            \"bookingDate\": \"2017-06-16\",\n" +
			"            \"reference\": \"\",\n" +
			"            \"transactionCode\": \"\",\n" +
			"            \"transactionSubCode\": \"\",\n" +
			"            \"proprietaryCode\": \"\",\n" +
			"            \"proprietarySubCode\": \"\",\n" +
			"            \"description\": \"BT MOBILE\",\n" +
			"            \"amount\": \"22.0\",\n" +
			"            \"currencyCode\": \"GBP\",\n" +
			"            \"type\": \"DEBIT\",\n" +
			"            \"status\": \"BOOKED\",\n" +
			"            \"merchant\": {\n" +
			"                \"name\": \"BT MOBILE\"\n" +
			"            }\n" +
			"        },\n" +
			"        {\n" +
			"            \"bankId\": 2,\n" +
			"            \"accountId\": \"fakeAcc77\",\n" +
			"            \"transactionId\": \"a5eb8fc9-c425-4e10-85a4-a937367206a9\",\n" +
			"            \"bookingDate\": \"2017-03-24\",\n" +
			"            \"reference\": \"\",\n" +
			"            \"transactionCode\": \"\",\n" +
			"            \"transactionSubCode\": \"\",\n" +
			"            \"proprietaryCode\": \"\",\n" +
			"            \"proprietarySubCode\": \"\",\n" +
			"            \"description\": \"BT PLC\",\n" +
			"            \"amount\": \"247.67\",\n" +
			"            \"currencyCode\": \"GBP\",\n" +
			"            \"type\": \"DEBIT\",\n" +
			"            \"status\": \"BOOKED\",\n" +
			"            \"merchant\": {\n" +
			"                \"name\": \"BT PLC\"\n" +
			"            }\n" +
			"        },\n" +
			"        {\n" +
			"            \"bankId\": 2,\n" +
			"            \"accountId\": \"fakeAcc77\",\n" +
			"            \"transactionId\": \"afda4441-63bf-4e05-9a82-f6849e265ff2\",\n" +
			"            \"bookingDate\": \"2017-01-13\",\n" +
			"            \"reference\": \"\",\n" +
			"            \"transactionCode\": \"\",\n" +
			"            \"transactionSubCode\": \"\",\n" +
			"            \"proprietaryCode\": \"\",\n" +
			"            \"proprietarySubCode\": \"\",\n" +
			"            \"description\": \"COSTA COFFEE\",\n" +
			"            \"amount\": \"13.9\",\n" +
			"            \"currencyCode\": \"GBP\",\n" +
			"            \"type\": \"DEBIT\",\n" +
			"            \"status\": \"BOOKED\",\n" +
			"            \"merchant\": {\n" +
			"                \"name\": \"COSTA COFFEE\"\n" +
			"            }\n" +
			"        },\n" +
			"        {\n" +
			"            \"bankId\": 2,\n" +
			"            \"accountId\": \"fakeAcc77\",\n" +
			"            \"transactionId\": \"b5420564-2f4e-4b10-bba9-274eab8d1b5f\",\n" +
			"            \"bookingDate\": \"2018-07-22\",\n" +
			"            \"reference\": \"\",\n" +
			"            \"transactionCode\": \"\",\n" +
			"            \"transactionSubCode\": \"\",\n" +
			"            \"proprietaryCode\": \"\",\n" +
			"            \"proprietarySubCode\": \"\",\n" +
			"            \"description\": \"LOW BALANCE\",\n" +
			"            \"amount\": \"15000.0\",\n" +
			"            \"currencyCode\": \"GBP\",\n" +
			"            \"type\": \"DEBIT\",\n" +
			"            \"status\": \"BOOKED\",\n" +
			"            \"merchant\": {\n" +
			"                \"name\": \"Starbucks\"\n" +
			"            }\n" +
			"        },\n" +
			"        {\n" +
			"            \"bankId\": 2,\n" +
			"            \"accountId\": \"fakeAcc77\",\n" +
			"            \"transactionId\": \"bbf8df70-509b-405b-9206-9fc48d78ea20\",\n" +
			"            \"bookingDate\": \"2018-07-25\",\n" +
			"            \"reference\": \"\",\n" +
			"            \"transactionCode\": \"\",\n" +
			"            \"transactionSubCode\": \"\",\n" +
			"            \"proprietaryCode\": \"\",\n" +
			"            \"proprietarySubCode\": \"\",\n" +
			"            \"description\": \"qa test2\",\n" +
			"            \"amount\": \"10000.0\",\n" +
			"            \"currencyCode\": \"GBP\",\n" +
			"            \"type\": \"DEBIT\",\n" +
			"            \"status\": \"BOOKED\",\n" +
			"            \"merchant\": {\n" +
			"                \"name\": \"WARRIORS\"\n" +
			"            }\n" +
			"        },\n" +
			"        {\n" +
			"            \"bankId\": 2,\n" +
			"            \"accountId\": \"fakeAcc77\",\n" +
			"            \"transactionId\": \"c98c9faa-c000-4efe-a5a4-9f09b65aa1ec\",\n" +
			"            \"bookingDate\": \"2018-03-03\",\n" +
			"            \"reference\": \"\",\n" +
			"            \"transactionCode\": \"\",\n" +
			"            \"transactionSubCode\": \"\",\n" +
			"            \"proprietaryCode\": \"\",\n" +
			"            \"proprietarySubCode\": \"\",\n" +
			"            \"description\": \"BT MOBILE\",\n" +
			"            \"amount\": \"30.0\",\n" +
			"            \"currencyCode\": \"GBP\",\n" +
			"            \"type\": \"DEBIT\",\n" +
			"            \"status\": \"BOOKED\",\n" +
			"            \"merchant\": {\n" +
			"                \"name\": \"BT MOBILE\"\n" +
			"            }\n" +
			"        },\n" +
			"        {\n" +
			"            \"bankId\": 2,\n" +
			"            \"accountId\": \"fakeAcc77\",\n" +
			"            \"transactionId\": \"d0365c70-9695-4aab-be75-50de44a5b39f\",\n" +
			"            \"bookingDate\": \"2017-03-03\",\n" +
			"            \"reference\": \"\",\n" +
			"            \"transactionCode\": \"\",\n" +
			"            \"transactionSubCode\": \"\",\n" +
			"            \"proprietaryCode\": \"\",\n" +
			"            \"proprietarySubCode\": \"\",\n" +
			"            \"description\": \"Non-Sterling Page Fee\",\n" +
			"            \"amount\": \"4.77\",\n" +
			"            \"currencyCode\": \"GBP\",\n" +
			"            \"type\": \"DEBIT\",\n" +
			"            \"status\": \"BOOKED\",\n" +
			"            \"merchant\": {\n" +
			"                \"name\": \"HSBC PLC\"\n" +
			"            }\n" +
			"        },\n" +
			"        {\n" +
			"            \"bankId\": 2,\n" +
			"            \"accountId\": \"fakeAcc77\",\n" +
			"            \"transactionId\": \"d6a43cff-de8b-473d-a00e-903c73b3c974\",\n" +
			"            \"bookingDate\": \"2018-03-20\",\n" +
			"            \"reference\": \"\",\n" +
			"            \"transactionCode\": \"\",\n" +
			"            \"transactionSubCode\": \"\",\n" +
			"            \"proprietaryCode\": \"\",\n" +
			"            \"proprietarySubCode\": \"\",\n" +
			"            \"description\": \"PAYROLL LAURA TESTER\",\n" +
			"            \"amount\": \"6666.03\",\n" +
			"            \"currencyCode\": \"GBP\",\n" +
			"            \"type\": \"DEBIT\",\n" +
			"            \"status\": \"BOOKED\",\n" +
			"            \"merchant\": {\n" +
			"                \"name\": \"PAYROLL\"\n" +
			"            }\n" +
			"        },\n" +
			"        {\n" +
			"            \"bankId\": 2,\n" +
			"            \"accountId\": \"fakeAcc77\",\n" +
			"            \"transactionId\": \"ee15ba88-6c8f-4b0c-92ad-d2cfbe660fcb\",\n" +
			"            \"bookingDate\": \"2018-07-22\",\n" +
			"            \"reference\": \"\",\n" +
			"            \"transactionCode\": \"\",\n" +
			"            \"transactionSubCode\": \"\",\n" +
			"            \"proprietaryCode\": \"\",\n" +
			"            \"proprietarySubCode\": \"\",\n" +
			"            \"description\": \"LOSING MONEE\",\n" +
			"            \"amount\": \"6000.0\",\n" +
			"            \"currencyCode\": \"GBP\",\n" +
			"            \"type\": \"DEBIT\",\n" +
			"            \"status\": \"BOOKED\",\n" +
			"            \"merchant\": {\n" +
			"                \"name\": \"bikes\"\n" +
			"            }\n" +
			"        },\n" +
			"        {\n" +
			"            \"bankId\": 2,\n" +
			"            \"accountId\": \"fakeAcc77\",\n" +
			"            \"transactionId\": \"ee2bd1a9-afcc-4878-a2d8-bc10414a5893\",\n" +
			"            \"bookingDate\": \"2018-01-02\",\n" +
			"            \"reference\": \"\",\n" +
			"            \"transactionCode\": \"\",\n" +
			"            \"transactionSubCode\": \"\",\n" +
			"            \"proprietaryCode\": \"\",\n" +
			"            \"proprietarySubCode\": \"\",\n" +
			"            \"description\": \"INT'L 003243488 GITHUB.COM\",\n" +
			"            \"amount\": \"7230.1\",\n" +
			"            \"currencyCode\": \"GBP\",\n" +
			"            \"type\": \"DEBIT\",\n" +
			"            \"status\": \"BOOKED\",\n" +
			"            \"merchant\": {\n" +
			"                \"name\": \"GITHUB.COM\"\n" +
			"            }\n" +
			"        },\n" +
			"        {\n" +
			"            \"bankId\": 2,\n" +
			"            \"accountId\": \"fakeAcc77\",\n" +
			"            \"transactionId\": \"eecc194c-ad83-45d7-8dfb-77f84d1721d2\",\n" +
			"            \"bookingDate\": \"2018-07-24\",\n" +
			"            \"reference\": \"\",\n" +
			"            \"transactionCode\": \"\",\n" +
			"            \"transactionSubCode\": \"\",\n" +
			"            \"proprietaryCode\": \"\",\n" +
			"            \"proprietarySubCode\": \"\",\n" +
			"            \"description\": \"TEST MONEY MONEE\",\n" +
			"            \"amount\": \"6000.0\",\n" +
			"            \"currencyCode\": \"GBP\",\n" +
			"            \"type\": \"DEBIT\",\n" +
			"            \"status\": \"BOOKED\",\n" +
			"            \"merchant\": {\n" +
			"                \"name\": \"bikes\"\n" +
			"            }\n" +
			"        },\n" +
			"        {\n" +
			"            \"bankId\": 2,\n" +
			"            \"accountId\": \"fakeAcc77\",\n" +
			"            \"transactionId\": \"f73921e2-8dd3-48fc-8096-cf7b8f10d5b7\",\n" +
			"            \"bookingDate\": \"2017-12-25\",\n" +
			"            \"reference\": \"\",\n" +
			"            \"transactionCode\": \"\",\n" +
			"            \"transactionSubCode\": \"\",\n" +
			"            \"proprietaryCode\": \"\",\n" +
			"            \"proprietarySubCode\": \"\",\n" +
			"            \"description\": \"SENIOR DEBT INTEREST PMT\",\n" +
			"            \"amount\": \"1875.0\",\n" +
			"            \"currencyCode\": \"GBP\",\n" +
			"            \"type\": \"DEBIT\",\n" +
			"            \"status\": \"BOOKED\",\n" +
			"            \"merchant\": {\n" +
			"                \"name\": \"BARCLAYS PLC\"\n" +
			"            }\n" +
			"        },\n" +
			"        {\n" +
			"            \"bankId\": 2,\n" +
			"            \"accountId\": \"fakeAcc77\",\n" +
			"            \"transactionId\": \"3dacfa8d-f627-4742-bdd2-0f3194fa9e91\",\n" +
			"            \"bookingDate\": \"2017-03-28\",\n" +
			"            \"reference\": \"\",\n" +
			"            \"transactionCode\": \"\",\n" +
			"            \"transactionSubCode\": \"\",\n" +
			"            \"proprietaryCode\": \"\",\n" +
			"            \"proprietarySubCode\": \"\",\n" +
			"            \"description\": \"SOME DESIGN LIMITED\",\n" +
			"            \"amount\": \"750.0\",\n" +
			"            \"currencyCode\": \"GBP\",\n" +
			"            \"type\": \"CREDIT\",\n" +
			"            \"status\": \"BOOKED\",\n" +
			"            \"merchant\": {\n" +
			"                \"name\": \"SOME DESIGN LIMITED\"\n" +
			"            }\n" +
			"        },\n" +
			"        {\n" +
			"            \"bankId\": 2,\n" +
			"            \"accountId\": \"fakeAcc77\",\n" +
			"            \"transactionId\": \"41c30ff7-877b-45a1-ae2d-b996f0f7aebe\",\n" +
			"            \"bookingDate\": \"2017-09-24\",\n" +
			"            \"reference\": \"\",\n" +
			"            \"transactionCode\": \"\",\n" +
			"            \"transactionSubCode\": \"\",\n" +
			"            \"proprietaryCode\": \"\",\n" +
			"            \"proprietarySubCode\": \"\",\n" +
			"            \"description\": \"SOME DESIGN LIMITED\",\n" +
			"            \"amount\": \"612.49\",\n" +
			"            \"currencyCode\": \"GBP\",\n" +
			"            \"type\": \"CREDIT\",\n" +
			"            \"status\": \"BOOKED\",\n" +
			"            \"merchant\": {\n" +
			"                \"name\": \"SOME DESIGN LIMITED\"\n" +
			"            }\n" +
			"        },\n" +
			"        {\n" +
			"            \"bankId\": 2,\n" +
			"            \"accountId\": \"fakeAcc77\",\n" +
			"            \"transactionId\": \"4f0784df-f91e-4e27-abd0-35ddb82f76eb\",\n" +
			"            \"bookingDate\": \"2017-10-28\",\n" +
			"            \"reference\": \"\",\n" +
			"            \"transactionCode\": \"\",\n" +
			"            \"transactionSubCode\": \"\",\n" +
			"            \"proprietaryCode\": \"\",\n" +
			"            \"proprietarySubCode\": \"\",\n" +
			"            \"description\": \"LITTLE BUSINESS PLC\",\n" +
			"            \"amount\": \"112000.0\",\n" +
			"            \"currencyCode\": \"GBP\",\n" +
			"            \"type\": \"CREDIT\",\n" +
			"            \"status\": \"BOOKED\",\n" +
			"            \"merchant\": {\n" +
			"                \"name\": \"LITTLE BUSINESS PLC\"\n" +
			"            }\n" +
			"        },\n" +
			"        {\n" +
			"            \"bankId\": 2,\n" +
			"            \"accountId\": \"fakeAcc77\",\n" +
			"            \"transactionId\": \"50cfaae7-ffbb-4cc9-a858-513aa2bd743e\",\n" +
			"            \"bookingDate\": \"2018-07-22\",\n" +
			"            \"reference\": \"\",\n" +
			"            \"transactionCode\": \"\",\n" +
			"            \"transactionSubCode\": \"\",\n" +
			"            \"proprietaryCode\": \"\",\n" +
			"            \"proprietarySubCode\": \"\",\n" +
			"            \"description\": \"BIG MONEEZ\",\n" +
			"            \"amount\": \"7500.0\",\n" +
			"            \"currencyCode\": \"GBP\",\n" +
			"            \"type\": \"CREDIT\",\n" +
			"            \"status\": \"BOOKED\",\n" +
			"            \"merchant\": {\n" +
			"                \"name\": \"prufrock\"\n" +
			"            }\n" +
			"        },\n" +
			"        {\n" +
			"            \"bankId\": 2,\n" +
			"            \"accountId\": \"fakeAcc77\",\n" +
			"            \"transactionId\": \"73cace36-ef6d-4a7c-9541-73238d24eb6d\",\n" +
			"            \"bookingDate\": \"2018-07-25\",\n" +
			"            \"reference\": \"\",\n" +
			"            \"transactionCode\": \"\",\n" +
			"            \"transactionSubCode\": \"\",\n" +
			"            \"proprietaryCode\": \"\",\n" +
			"            \"proprietarySubCode\": \"\",\n" +
			"            \"description\": \"qa test3\",\n" +
			"            \"amount\": \"6000.0\",\n" +
			"            \"currencyCode\": \"GBP\",\n" +
			"            \"type\": \"CREDIT\",\n" +
			"            \"status\": \"BOOKED\",\n" +
			"            \"merchant\": {\n" +
			"                \"name\": \"ROCKETS\"\n" +
			"            }\n" +
			"        },\n" +
			"        {\n" +
			"            \"bankId\": 2,\n" +
			"            \"accountId\": \"fakeAcc77\",\n" +
			"            \"transactionId\": \"baa36afb-6c82-4a52-ade1-b7e40f59146f\",\n" +
			"            \"bookingDate\": \"2018-07-24\",\n" +
			"            \"reference\": \"\",\n" +
			"            \"transactionCode\": \"\",\n" +
			"            \"transactionSubCode\": \"\",\n" +
			"            \"proprietaryCode\": \"\",\n" +
			"            \"proprietarySubCode\": \"\",\n" +
			"            \"description\": \"money in\",\n" +
			"            \"amount\": \"6000.0\",\n" +
			"            \"currencyCode\": \"GBP\",\n" +
			"            \"type\": \"CREDIT\",\n" +
			"            \"status\": \"BOOKED\",\n" +
			"            \"merchant\": {\n" +
			"                \"name\": \"Starbucks\"\n" +
			"            }\n" +
			"        },\n" +
			"        {\n" +
			"            \"bankId\": 2,\n" +
			"            \"accountId\": \"fakeAcc77\",\n" +
			"            \"transactionId\": \"ea58e941-42b0-4e57-ad37-067be59bd0e5\",\n" +
			"            \"bookingDate\": \"2018-07-24\",\n" +
			"            \"reference\": \"\",\n" +
			"            \"transactionCode\": \"\",\n" +
			"            \"transactionSubCode\": \"\",\n" +
			"            \"proprietaryCode\": \"\",\n" +
			"            \"proprietarySubCode\": \"\",\n" +
			"            \"description\": \"Nisa\",\n" +
			"            \"amount\": \"7500.0\",\n" +
			"            \"currencyCode\": \"GBP\",\n" +
			"            \"type\": \"CREDIT\",\n" +
			"            \"status\": \"BOOKED\",\n" +
			"            \"merchant\": {\n" +
			"                \"name\": \"prufrock\"\n" +
			"            }\n" +
			"        }\n" +
			"    ],\n" +
			"    \"pageNo\": 1,\n" +
			"    \"noPages\": 1\n" +
			"}";



	@Test
	public void contextLoads() {
	}

	@Autowired
	TransactionService tServ;

	@Test
	public void testProcessOneSetOfTransactions(){
		tServ.clear();
		tServ.importPageOfTransactions(transactions);
		List<Result> allResults = tServ.getAllTransactions();
		log.info("All transactions  = "+ allResults.size());
		assert(allResults.size()==33);
	}



	@Test
	public void testProcessTwoSetsOfTransactions(){
		tServ.clear();
		tServ.importPageOfTransactions(transactions);
		tServ.importPageOfTransactions(transactions);
		List<Result> allResults = tServ.getAllTransactions();
		log.info("All transactions  = "+ allResults.size());
		assert(allResults.size()==66);
	}

	@Autowired
	CategorisingService cServ;

	@Test
	public void testSimpleCoffee(){
		CategoryConfidence category = cServ.categorise("coffee");
		assert(category.getCategory().getName().equalsIgnoreCase("coffee"));
	}

	@Test
	public void testCoffeeAndTravelCategorisation(){
		int coffees = 0;
		int travel = 0;
		tServ.clear();
		tServ.importPageOfTransactions(transactions);

		NumberFormat defaultFormat = NumberFormat.getPercentInstance();
		defaultFormat.setMinimumFractionDigits(1);

		for (Result result : tServ.getAllTransactions()){
			CategoryConfidence category = cServ.categorise(result.getDescription());
			if(category != null){
				if (category.getCategory().getName().equals("coffee")){
					coffees++;
				} else if (category.getCategory().getName().equals("travel")){
					travel++;
				}
				log.info("Categorising:" + result.getDescription() +
						" as " + category.getCategory().getName() +
						" with confidence " + defaultFormat.format(Math.min(category.getConfidence(),1.0)));
			} else {
				log.info("Not Categorising:" + result.getDescription());
			}
		}
		log.info("Coffee="+coffees);
		log.info("Travel="+travel);
		assert(coffees==5);
		assert(travel==2);

	}
}
