package com.fractallabs.zuulserver;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;

@Component
public class CustomZuulFilter extends ZuulFilter {

	@Override
	public boolean shouldFilter() {
		return true;
	}

	@Override
	public String filterType() {
		return "pre";
	}

	@Override
	public int filterOrder() {
		return 99;
	}

	@Override
	public Object run() {
		System.out.println("pre filter");
		RequestContext ctx = RequestContext.getCurrentContext();
		HttpServletRequest request = ctx.getRequest();
		String header;
		Boolean isAuthorised = false;
		Enumeration<String> headers = request.getHeaderNames();
		while (headers.hasMoreElements()) {
			header = headers.nextElement();
			if (header.equalsIgnoreCase("authorization")) {
				Jwt jwt = JwtReader.decodeJWT(request.getHeader(header));
				System.out.println(jwt.getBody());
				ctx.addZuulRequestHeader(header, request.getHeader(header));
				System.out.println("added "+header+":"+request.getHeader(header));
				isAuthorised = true;
			} else {
				ctx.addZuulRequestHeader(header,request.getHeader(header));
				System.out.println("Copying header "+header +":"+request.getHeader(header) );
			}
		}
		if (!isAuthorised){
			ctx.setSendZuulResponse(false); //This makes request not forwarding to micro services
			ctx.setResponseStatusCode(HttpStatus.UNAUTHORIZED.value());;
			ctx.getResponse().setContentType("application/json");
		} else {
			ctx.setSendZuulResponse(true);
			ctx.addZuulRequestHeader("x-secure-token","fvklvaosihOHFoih0893247283hrwnhsdjhcksjdfh4u8yfp9784fh49iudehf");
			System.out.println("ok");
		}
		return null;
	}
}
