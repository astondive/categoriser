package com.fractallabs.zuulserver;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Jwt {
	String header;
	String Body;
	String signature;
}
