package com.fractallabs.zuulserver;

import org.apache.commons.codec.binary.Base64;

public class JwtReader {


	public static Jwt decodeJWT(String jwt) {
		System.out.println("------------ Decode JWT ------------");
		String[] split_string = jwt.split("\\.");
		String base64EncodedHeader = split_string[0];
		String base64EncodedBody = split_string[1];
		String base64EncodedSignature = split_string[2];

		System.out.println("~~~~~~~~~ JWT Header ~~~~~~~");
		Base64 base64Url = new Base64(true);
		String header = new String(base64Url.decode(base64EncodedHeader));
		System.out.println("JWT Header : " + header);


		System.out.println("~~~~~~~~~ JWT Body ~~~~~~~");
		String body = new String(base64Url.decode(base64EncodedBody));
		System.out.println("JWT Body : "+body);

		return new Jwt(header,body, base64EncodedSignature);
	}

}
